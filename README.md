# FASTA sequence query application

This program searches through a FASTA-file and returns all the found sequences.

The sequences can be filtered on the header information (e.g. Acession number, name and organism).
The output format can be modified as well to get a summary of
the found sequences or in a csv format.


##Installation


* Download from source (git clone, zipped package)
* Run the jar file in root directory:


```
java -jar SeqQuery-1.0.jar
```

##Usage
```
java -jar SeqQuery-1.0.jar -i <INFILE> [-id ID] [-d description] [-o organism]
                 [-s | -c separator | -r regex_pattern | -p prosite_pattern]
```

####Main arguments
* -i | --input <INFILE> (**required**)  The FASTA file to read (supported files end with .fa .fna or .faa).
* -h | --help   , show a help page, with program usage.
####Options
#####Filters
* -id | --find_id <ACCNO>    show all the sequences with an accession number containing the id.
* -d | --find_description (WILDCARD_STRING)  show all the sequences with a description containing the wildcard string (regex supported).
* -o | --find_organism (ORGANISM_NAME)   show all the sequences with organism containing given name.
* -r | --find_regex <REGEX_PATTERN> finds the given regex pattern in the sequence, and returns the found pattern and position in sequence. (**cannot** be used with any output modifiers)
* -p | --find_prosite <PROSITE_PATTERN> finds the given prosite pattern in the sequence, return the pattern and found position.
See [here](http://www.hpa-bioinfotools.org.uk/ps_scan/PS_SCAN_PATTERN_SYNTAX.html) for PROSITE pattern examples. (**cannot** be used with any output modifiers)
#####Output modifiers
* -s | --summary    , summarize the output results
* -c | --to_csv <SEPARATOR> Generates a csv listing with the given separator as separator.

When no output modifier is used, the header and the sequence itself is shown.

#Output format headers
When using one of the three output modifiers below, the following columns are present:

to_csv:

* ACCNO: First accession
* NAME: Name / description
* ORGANISM: Organism
* TYPE: Type (DNA, RNA, Protein)
* LENGTH: Length
* MOL_WEIGHT: Molecular weight


find_regex and find_prosite:

* ACCNO: First accession
* NAME: Name / description
* ORGANISM: Organism
* TYPE: Type (DNA, RNA, Protein)
* POSITION: Start position of match
* SEQ: Actual sequence of match

##Use cases

#####Summary use case example
```
java -jar SeqQuery-1.0.jar --input data/fhit.faa --summary
File:                       fhit.faa
Sequence types:             PROTEIN
Number of sequences:        1830
Average sequence length:    195.70
```

#####Find_id use case example
```
java -jar SeqQuery-1.0.jar --input data/fhit.faa --find_id "gi|312377296"
>gi|312377296|gb|EFR24160.1| hypothetical protein AND_11452 [Anopheles darlingi]
MRLSEPLTGPLVQQYKQLAREHNVWLSLGGIHESITDATGPEKEVQRIFNTHIVIDNAGELVATYRKLHM
FNVVTPEFKFRESDTVQSGATVVAPIESPIGRIGLQICYDVRFPEVSTILRKQGAEILTYPSAFAVSTGR
AHWEVLLRARAIENQCFVVAAAQIGFHNKKRESYGHAMVVNPWGTILAEADPTLDLDVVYAELDYDKLDN
TRANMPCFDHRRNDVYNLTPLKELRHTNDSSKKEEYRFGSFVIEPETIFYESEHCFAFTNIRCVVPGQIS
DFFQVVCKVQRAAERLYDATSSTITVQDGPDAGQTVFHVHCHVMPRHVGDFPENDQIYGELNRHDKEPDR
PRRPLAEMMTEATRYRMELSRLGL
```

#####Find_description use case example
```
java -jar SeqQuery-1.0.jar --input data/fhit.faa --find_description "[Hh]istidine"
>gi|3243136|gb|AAC23967.1| fragile histidine triad protein [Mus musculus]
MSFRFGQHLIKPSVVFLKTELSFALVNRKPVVPGHVLVCPLRPVERFRDLHPDEVADLFQVTQRVGTVVE
KHFQGTSITFSMQDGPEAGQTVKHVHVHVLPRKAGDFPRNDNIYDE
>gi|9587672|gb|AAF89328.1|AF170064_1 fragile histidine triad protein [Rattus norvegicus]
MSFKFGQHLIKPSVVFLKTELSFALVNRKPVVPGHVLMCPLRPVERFRDLRPDEVADLFQVTQRVGTVVE
KHFQGTSITFSMQDGPEAGQTVKHVHVHILPRKSGDFRRNDNIYDELQKHDREEEDSPAFWRSEEEMAAE
AEVLRAYFQA
>gi|3264590|gb|AAC24566.1| fragile histidine triad protein [Mus musculus]
MSFRFGQHLIKPSVVFLKTELSFALVNRKPVVPGHVLVCPLRPVERFRDLHPDEVADLFQVTQRVGTVVE
KHFQGTSITFSMQDGPEAGQTVKHVHVHVLPRKAGDFPRNDNIYDELQKHDREEEDSPAFWRSEKEMAAE
AEALRVYFQA
...710 more matches omitted
```

#####find_organism use case example
```
java -jar SeqQuery-1.0.jar --input data/fhit.faa --find_organism "Brugia malayi"
>gi|158593527|gb|EDP32122.1| hydrolase, carbon-nitrogen family protein [Brugia malayi]
MPGCTLLSTAIRRQFSTLMNEKRSLIAVCQLTATNDLEANFEVAKCMMKRAKERKAKMVFFPECFDYVGE
SRNEIEALALSENDDYISRYRTCAKEYGLWLSLGGFHQKDPAGLRKPFNTHIIVDDSGKTRGIYRKLHLF
DLDIPGKVRLVESEFSSRGDEISKPVCTPVGNVAMSICYDLRFAELALWYRMNGAHVLTYPSAFTVDTGC
AHWEILLRTRAVETQCYVVAAAQTGKHNDKRSSYGHAMVVDPWGAVVAQCSETIDVCFAEISLNYLDEVR
KLQPVFEHRRSDLYSLIVVQGNEIGNKAYMFGSHSVPPEHVFYRSTYTFCFVNRSPVLPGHVLVCPVRNV
KRLTELSHTETSDLFITAKRIQTMLEDYYKATSSTVCVQDGPEAGQTVSHVHVHILPRKKNDFGSDPDNI
YRELADHDKIGKKRFRNKEEMQNEANVYRCLLVNGEAE
>gi|170589105|ref|XP_001899314.1| hydrolase, carbon-nitrogen family protein [Brugia malayi]
MPGCTLLSTAIRRQFSTLMNEKRSLIAVCQLTATNDLEANFEVAKCMMKRAKERKAKMVFFPECFDYVGE
SRNEIEALALSENDDYISRYRTCAKEYGLWLSLGGFHQKDPAGLRKPFNTHIIVDDSGKTRGIYRKLHLF
DLDIPGKVRLVESEFSSRGDEISKPVCTPVGNVAMSICYDLRFAELALWYRMNGAHVLTYPSAFTVDTGC
AHWEILLRTRAVETQCYVVAAAQTGKHNDKRSSYGHAMVVDPWGAVVAQCSETIDVCFAEISLNYLDEVR
KLQPVFEHRRSDLYSLIVVQGNEIGNKAYMFGSHSVPPEHVFYRSTYTFCFVNRSPVLPGHVLVCPVRNV
KRLTELSHTETSDLFITAKRIQTMLEDYYKATSSTVCVQDGPEAGQTVSHVHVHILPRKKNDFGSDPDNI
YRELADHDKIGKKRFRNKEEMQNEANVYRCLLVNGEAE
```

#####Find_regex use case example
```
java -jar SeqQuery-1.0.jar --input data/fhit_sample.faa --find_regex "H.H.H[VI]"
ACCNO;NAME;ORGANISM;TYPE;POSITION;SEQ
gi|21595364;FHIT protein;Homo sapiens;PROTEIN;93;HVHVHV
gi|15215093;Fhit protein;Mus musculus;PROTEIN;93;HVHVHV
gi|151554847;FHIT protein;Bos taurus;PROTEIN;93;HVHVHI
gi|256665365;fragile histidine triad protein;Ovis aries;PROTEIN;93;HVHIHV
gi|11120730;bis(5'-adenosyl)-triphosphatase;Rattus norvegicus;PROTEIN;96;HVHVHI
```

#####Find_prosite use case example
```
java -jar SeqQuery-1.0.jar --input data/fhit_sample.faa --find_prosite "H-x-H-x-H-[VI]"
ACCNO;NAME;ORGANISM;TYPE;POSITION;SEQ
gi|21595364;FHIT protein;Homo sapiens;PROTEIN;93;HVHVHV
gi|15215093;Fhit protein;Mus musculus;PROTEIN;93;HVHVHV
gi|151554847;FHIT protein;Bos taurus;PROTEIN;93;HVHVHI
gi|256665365;fragile histidine triad protein;Ovis aries;PROTEIN;93;HVHIHV
gi|11120730;bis(5'-adenosyl)-triphosphatase;Rattus norvegicus;PROTEIN;96;HVHVHI
```


However multiple filters and output modifier can be used together as shown below.

#####To_csv use case example
```
java -jar SeqQuery-1.0.jar --input data/fhit.faa --find_organism "Brugia malayi" --to_csv ";"
ACCNO;NAME;ORGANISM;TYPE;LENGTH;MOL_WEIGHT
gi|158593527;hydrolase, carbon-nitrogen family protein;Brugia malayi;PROTEIN;458;60233.0
gi|170589105;hydrolase, carbon-nitrogen family protein;Brugia malayi;PROTEIN;458;60233.0
```
#####Multiple filters use case example
```
java -jar SeqQuery-1.0.jar --input data/fhit.faa --find_organism "Homo" --find_description "[Hh]ydrolase" --summary
File:                       fhit.faa
Sequence types:             PROTEIN
Number of sequences:        8
Average sequence length:    63.88
```