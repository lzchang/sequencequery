/*
 * Copyright (c) 2017. Lin Chang [l.z.chang@st.hanze.nl].
 * All rights reserved.
 */

package nl.bioinf.lzchang.seqquery.model;

import java.util.HashMap;
import java.util.Locale;

/**
 * This class receives a sequence header and the sequence itself.
 * With those information the header can e.g. be split into Accession number, name and organism.
 * The sequence can be used to calculate molecular weight, and the type.
 * @Author Lin Chang [l.z.chang@st.hanze.nl]
 * @version 0.0.1
 */
public class Sequence {
    private String header;
    private String sequence;

    public Sequence(String header, String sequence) {
        this.header = header;
        this.sequence = sequence;
    }

    /**
     * In this method the sequence is checked for characters corresponding
     * to its sequence type. E.g. if sequence only contains G/A/T/C then its
     * a DNA sequence, G/A/U/C for RNA and protein for everything else.
     * @return sequenceType
     */
    public String getSequenceType() {
        String sequenceType;

        if (sequence.matches("[GATC]*")){
            sequenceType = "DNA";
        }
        else if (sequence.matches("[GAUC]*")){
            sequenceType = "RNA";
        } else {
            sequenceType = "PROTEIN";
        }
        return sequenceType;
    }

    /**
     * Here the molecular weight of the sequence is calculated in g/mol.
     * Undetermined amino acids (X) will be ignored.
     * molecular weights are obtained from http://www.webqc.org/aminoacids.php
     * @return totalWeight
     */
    public String getMolecularWeight(){
        double totalWeight = 0.0;
        HashMap<Character, Double> aminoAcidWeights = new HashMap<>();

        aminoAcidWeights.put('A', 89.0935);
        aminoAcidWeights.put('R', 174.2017);
        aminoAcidWeights.put('N', 132.1184);
        aminoAcidWeights.put('D', 133.1032);
        aminoAcidWeights.put('C', 121.1590);
        aminoAcidWeights.put('E', 147.1299);
        aminoAcidWeights.put('Q', 146.1451);
        aminoAcidWeights.put('G', 75.0669);
        aminoAcidWeights.put('H', 155.1552);
        aminoAcidWeights.put('I', 131.1736);
        aminoAcidWeights.put('L', 131.1736);
        aminoAcidWeights.put('K', 146.1882);
        aminoAcidWeights.put('M', 149.2124);
        aminoAcidWeights.put('F', 165.1900);
        aminoAcidWeights.put('P', 115.1310);
        aminoAcidWeights.put('S', 105.0930);
        aminoAcidWeights.put('T', 119.1197);
        aminoAcidWeights.put('W', 204.2262);
        aminoAcidWeights.put('Y', 181.1894);
        aminoAcidWeights.put('V', 117.1469);

        for (Character aa : sequence.toCharArray()){
            if (!aa.equals('X')) {
                totalWeight += aminoAcidWeights.get(aa);
            }}

        return String.format(Locale.US,"%.1f", totalWeight);
    }

    /**
     * Finds the name of the sequence in the header.
     * >gi|151554847|gb|AAI47994.1| FHIT protein [Bos taurus]
     * In this case the name occurs after fourth | and before the organism
     * @return Name
     */
    public String getName(){
        int occurrence = 0;
        int startPosition = 0;

        while (occurrence != 4){ // Find the fourth occurrence of | and use that as start of index
            startPosition = header.indexOf("|", startPosition+1);
            occurrence ++;
        }

        int endPosition = header.indexOf(" [");
        if (endPosition == -1){
            endPosition = header.length();
        }

        return header.substring(startPosition +1, endPosition).trim();
    }

    /**
     * Finds the Accession number of the sequence in the header
     * >gi|151554847|gb|AAI47994.1| FHIT protein [Bos taurus]
     * The accession number in this case is after the > till the second |
     * @return AccessionNumber
     */
    public String getAccessionNumber(){
        int start = header.indexOf(">") + 1;
        int end = header.indexOf("|", header.indexOf("|") + 1);

        return header.substring(start, end);
    }

    /**
     * Find the organism of the sequence in the header. The organism can be found
     * within brackets at the end of the header. If no organism is defined, NA is returned.
     * @return Organism
     */
    public String getOrganism(){
        int start = header.indexOf("[") + 1 ;

        if (start == 0){
            return "NA";
        }

        return header.substring(start, header.length() - 1);
    }

    /**
     * Return the actual sequence.
     * @return sequence
     */
    public String getSequence() {
        return sequence;
    }

    @Override
    public String toString() {
        return header + '\n' +
                sequence.replaceAll("(.{70})", "$1\n")
                ;
    }

}
