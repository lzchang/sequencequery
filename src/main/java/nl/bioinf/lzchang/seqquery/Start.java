/*
 * Copyright (c) 2017. Lin Chang [l.z.chang@st.hanze.nl].
 * All rights reserved.
 */

package nl.bioinf.lzchang.seqquery;

import nl.bioinf.lzchang.seqquery.io.CliParser;

/**
 * This class is used for instantiating the CLI parser class.
 * @Author Lin Chang [l.z.chang@st.hanze.nl]
 * @version 0.0.1
 */
public class Start {
    public static void main(String[] args) {
        CliParser cliparser = new CliParser(args);
        cliparser.parse();
    }
}