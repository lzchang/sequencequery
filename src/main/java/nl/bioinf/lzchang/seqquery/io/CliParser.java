/*
 * Copyright (c) 2017. Lin Chang [l.z.chang@st.hanze.nl].
 * All rights reserved.
 */

package nl.bioinf.lzchang.seqquery.io;

import nl.bioinf.lzchang.seqquery.model.Sequence;
import org.apache.commons.cli.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This is the CLI parser, which reads the command line and parses the information,
 * returning information which are relevant to the given arguments.
 * @Author Lin Chang [l.z.chang@st.hanze.nl]
 * @version 0.0.1
 */
public class CliParser {
    private Options options = new Options();
    private ArrayList<Sequence> fileContent = new ArrayList<>();
    private String[] args = null;
    private CommandLine cmd = null;
    private ArrayList<String> allowedExtensions = new ArrayList<>
            (Arrays.asList(".fa",".faa",".fna"));

    public CliParser(String[] args) {
        this.args = args;

        defineOptions();
    }

    /**
     * In this method all possible command-line options are defined.
     */
    private void defineOptions() {
        Option input = Option.builder("i")
                .hasArg()
                .longOpt("input")
                .desc("The file to search through, supported files have .fa, .faa, and .fna extension.")
                .argName("INFILE")
                .build();

        Option find_prosite = Option.builder("p")
                .hasArg()
                .longOpt("find_prosite")
                .desc("Will report all sequences that contain the Prosite " +
                        "pattern, with the location and actual sequence found.")
                .argName("PROSITE PATTERN")
                .build();

        Option find_regex = Option.builder("r")
                .hasArg()
                .longOpt("find_regex")
                .desc("Will report all sequences that contain the Regular expression " +
                        "pattern, with the location and actual sequence found.")
                .argName("REGEX_PATTERN")
                .build();

        Option find_organism = Option.builder("o")
                .hasArg()
                .longOpt("find_organism")
                .desc("Will report all sequences having this wildcard string (a regex pattern) in the organism name.")
                .argName("(PART OF) ORGANISM_NAME")
                .build();

        Option find_description = Option.builder("d")
                .hasArg()
                .longOpt("find_description")
                .desc("Will report all sequences having this wildcard string (a regex pattern) in the description / " +
                        "sequence name.")
                .argName("WILDCARD_STRING")
                .build();

        Option to_csv = Option.builder("c")
                .hasArg()
                .longOpt("to_csv")
                .desc("Generates a nicely formatted csv listing with these columns ands the given character as " +
                        "separator.")
                .argName("SEPARATOR")
                .build();

        Option find_id = Option.builder("id")
                .hasArg()
                .longOpt("find_id")
                .desc("Returns the sequence with the given name as-is.")
                .argName("ID")
                .build();

        Option summary = Option.builder("s")
                .longOpt("summary")
                .desc("Creates a textual summary of the parsed file: number of sequences, sequence type and average length.")
                .build();

        OptionGroup OutputGroup = new OptionGroup();
        OutputGroup.addOption(summary);
        OutputGroup.addOption(to_csv);
        OutputGroup.addOption(find_prosite);
        OutputGroup.addOption(find_regex);
        options.addOptionGroup(OutputGroup);
        options.addOption("h","help", false, "Prints the help.");
        options.addOption(input);
        options.addOption(find_organism);
        options.addOption(find_description);
        options.addOption(find_id);
    }

    /**
     * Read the command line and and uses the argument values for calling other methods.
     */
    public void parse(){
        DefaultParser parser = new DefaultParser();

        try {
            cmd = parser.parse(options, args);

            if (cmd.hasOption("help") || args.length == 0){
                help();
            } else {
                String inputFile = cmd.getOptionValue("input");

                if (allowedExtensions.contains(inputFile.substring(inputFile.indexOf(".")))) {
                    parseFile(inputFile);
                } else {
                    throw new IOException();
                }
                // These three options filters the results, remove object from fileContent if found
                for (Sequence item : new ArrayList<>(fileContent)) {
                    if (cmd.hasOption("find_id")) {
                        if (!findID(item, cmd.getOptionValue("id"))){ continue;}
                    }
                    if (cmd.hasOption("find_organism")) {
                        if (!findOrganism(item, cmd.getOptionValue("o"))){continue;}
                    }
                    if (cmd.hasOption("find_description")) {
                        findDescription(item, cmd.getOptionValue("d"));
                    }
                }
                // These options decide how output will look like.
                if (cmd.hasOption("to_csv")) {
                    System.out.println("ACCNO;NAME;ORGANISM;TYPE;LENGTH;MOL_WEIGHT");
                    fileContent.forEach(item -> createCsvFormat(item, cmd.getOptionValue("c")));
                } else if (cmd.hasOption("summary")) {
                    summary();
                } else if (cmd.hasOption("find_prosite") || cmd.hasOption("find_regex")) {
                    final Pattern regexPattern ;

                    System.out.println("ACCNO;NAME;ORGANISM;TYPE;POSITION;SEQ");
                    if (cmd.hasOption("find_prosite")){
                        regexPattern = MakePattern(cmd.getOptionValue("p"), true);
                    } else {
                        regexPattern = MakePattern(cmd.getOptionValue("r"));
                    }
                    fileContent.forEach(item -> findRegexInSequence(regexPattern, item));
                } else {
                    fileContent.forEach(System.out::println);
                }
            }

        } catch (AlreadySelectedException e){
            help();
            System.out.println("ERROR: only one of the given options are allowed.");
        } catch (ParseException | NullPointerException e) {
            help();
            System.out.println("ERROR: missing required options or arguments.");
        } catch (IOException | IndexOutOfBoundsException e){
            help();
            System.out.println("ERROR: " + cmd.getOptionValue("i") + " is not a supported file or does not exist.");
        }
    }

    /**
     * @see CliParser#MakePattern(String, boolean)
     * @param regex the regex pattern to compile.
     * @return regexPattern
     */
    private Pattern MakePattern(String regex) {
        return MakePattern(regex, false);
    }

    /**
     * Compile a regex pattern with the given pattern.
     * @param regex the regex pattern to compile.
     * @param prosite replaces certain characters to comply with prosite pattern if true.
     * @return regexPattern
     */
    private Pattern MakePattern(String regex, boolean prosite) {
        if (prosite){
            regex = regex.replace("-", "");
            regex = regex.replace("x", "[^BJOUXZ]");
            regex = regex.replace("{", "[^");
            regex = regex.replace("}", "]");
        }

        String pattern = ".*(" + regex + ").*";

        return Pattern.compile(pattern);
    }

    /**
     * Match the regex pattern on the sequence.
     * @param regex is the regex pattern.
     * @param item is the sequence object.
     */
    private void findRegexInSequence(Pattern regex, Sequence item) {
        Matcher m = regex.matcher(item.getSequence());
        if (m.find()){
            System.out.println(item.getAccessionNumber() + ";" + item.getName() +
                    ";" + item.getOrganism() + ";" + item.getSequenceType() +
                    ";" + item.getSequence().indexOf(m.group(1)) + ";" + m.group(1));
        }
    }

    /**
     * Print the Accession number, name, organism, type, length and molecular weight of
     * the sequences separated by the given separator.
     * @param item is the sequence object.
     * @param separator the separator character(s).
     */
    private void createCsvFormat(Sequence item, String separator) {
            System.out.println(item.getAccessionNumber() + separator + item.getName() +
                    separator + item.getOrganism() + separator + item.getSequenceType() +
                    separator + item.getSequence().length() + separator + item.getMolecularWeight());

    }

    /**
     * Searches the name of the sequence to find a match, if no match is found the item
     * is removed from the fileContent (filtered out).
     * @param item is the sequence object.
     * @param description the sequence description to filter on.
     */
    private void findDescription(Sequence item, String description) {
        if (!item.getName().matches(".*\\b" + description + "\\b.*")){
            fileContent.remove(item);
        }
    }

    /**
     * Looks for matches in the organism of the sequence. If there is no match the item will be
     * filtered out of the fileContent.
     * Sequences which do not contain a organism are not found.
     * @param item is the sequence object.
     * @param organism the sequence organism to filter on.
     * @return true if item found, else false.
     */
    private boolean findOrganism(Sequence item, String organism) {
        if (!item.getOrganism().contains(organism)){
            fileContent.remove(item);
            return false;
        } else {
            return true;
        }
    }

    /**
     * Look whether the Accession number is a match, if not remove the item from fileContent.
     * @param item is the sequence object.
     * @param id is the ID to filter on.
     * @return true if item found, else false.
     */
    private boolean findID(Sequence item, String id) {
        if (!item.getAccessionNumber().contains(id)){
            fileContent.remove(item);
            return false;
        } else {
            return true;
        }
    }

    /**
     * Prints the summary of the input file, this includes: the filename,
     * the sequence types, the number of sequences in the file and the average sequence length.
     */
    private void summary() {
        final int[] sequenceLength = {0};
        int numberOfSequences = fileContent.size();
        String filename = cmd.getOptionValue("i").substring(cmd.getOptionValue("i").indexOf("/")+1);

        fileContent.forEach((sequence)-> sequenceLength[0] += sequence.getSequence().length());

        System.out.println("File:\t\t\t\t" + filename);
        if (numberOfSequences == 0){
            System.out.println("Sequence types:\t\t\tNA");
        } else{
            System.out.println("Sequence types:\t\t\t" + fileContent.get(0).getSequenceType());
        }
        System.out.println("Number of sequences:\t\t" + numberOfSequences);
        System.out.println(String.format(Locale.US,"Average sequence length:\t%.1f",
                (double) sequenceLength[0] /numberOfSequences));
    }

    /**
     * In this method the input file is received from the parse method containing
     * the path location of the file which has to be read.
     * Here the header and the sequences are split and put into a Sequence object,
     * every sequence is appended to fileContent and returned.
     * @param fileToRead is the input file which needs to be parsed.
     * @throws IOException when input file is not valid.
     */
    private void parseFile(String fileToRead) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(fileToRead));
        StringBuilder sequenceToString = new StringBuilder();
        String header = "";
        String line;

        while((line = reader.readLine()) != null) {
            if (line.startsWith(">")){
                if (!header.isEmpty()){ // skip the first occurrence
                    fileContent.add(new Sequence(header, sequenceToString.toString()));
                    sequenceToString.setLength(0);                }
                header = line;
            } else { // combine multiple lines of sequences into a string
                sequenceToString.append(line);
            }
        }
        fileContent.add(new Sequence(header, sequenceToString.toString()));

        reader.close();
    }

    /**
     * Print the help with the usage of the program.
     */
    private void help(){
        HelpFormatter helpformatter = new HelpFormatter();
        helpformatter.printHelp("java -jar SeqQuery.jar -i <INFILE> [-id ID] [-d description] [-o organism] " +
                "[-s | -c separator | -r regex_pattern | -p prosite_pattern]", options);
    }
}